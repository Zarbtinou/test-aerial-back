# Installation

For install and launch the project , please follow this instructions:

Clone the repository

    git clone https://gitlab.com/Zarbtinou/test-aerial-back.git

Switch to the repo folder

    cd test-aerial-back

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

## Don't forget to create the database in mysql cli or with a part logiciel like PhpMyAdmin or other

Run the database migrations

    php artisan migrate

Start the local development server

    php -S localhost:3000 -t public

You can now access the server at http://localhost:3000
