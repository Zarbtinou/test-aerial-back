<?php

namespace App\Http\Controllers;
use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function create(Request $request) {

        $transaction = new Transaction();

        $transaction->reference = time();
        $transaction->product_ids = $request->product_ids;
        $transaction->amount = $request->amount;

        $transaction->save();

        return response()->json("Transaction successfully created");

    }
}
